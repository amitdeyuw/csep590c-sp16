"use strict";
// This uses D3 to render images
function refresh(data){
	var svg = d3.select("svg")
	var imgs = svg.selectAll("image").data(data);
	imgs.enter()
	.append("image")
	.attr("xlink:href", function(d){return d["imagePath"]})
	.attr("width", function(d){return d["width"]})
	.attr("height",function(d){return d["height"]})
	.attr("x", function(d){return d["x"]})
	.attr("y",function(d){return svg[0][0].clientHeight-d["y"]-d["height"]})
	.attr("preserveAspectRatio","none");
}

// This is the base class for all AST
function ASTNode(type) {
    this.type = type;
}

ASTNode.prototype = {};

function Blank() {
    ASTNode.call(this, "Blank");
}

// Place holder for Blank Node
Blank.prototype = Object.create(ASTNode.prototype);

Blank.prototype.render = function(data,x,y,scale,boundX,boundY) {
}

Blank.prototype.getWidth = function() {
	return 0;
}

Blank.prototype.getHeigth = function() {
	return 0;
}

// Render a single image
// This is also a terminal node
// Actual Draw only happens only for this node
function Single(imagePath,width,height) {
    ASTNode.call(this, "Single");
	this.imagePath = imagePath;
	this.width = width;
	this.height = height;
}

Single.prototype = Object.create(ASTNode.prototype);

Single.prototype.render = function(data,x,y,scale,boundX,boundY) {
	var width=this.width*scale;
	var height=this.height*scale;
	data.push({imagePath:this.imagePath,width:width,height:height,x:x,y:y});
	refresh(data);
}

Single.prototype.getHeigth = function() {
	return this.height;
}

Single.prototype.getWidth = function() {
	return this.width;
}

function HorizontalPair(first, second) {
    ASTNode.call(this, "HorizontalPair");
    this.first = first;
    this.second = second;
}

// Represent a pair of objects place horizontally next to each other
HorizontalPair.prototype = Object.create(ASTNode.prototype);
HorizontalPair.prototype.render = function(data,x,y,scale,boundX,boundY) {
	this.first.render(data,x+this.second.getWidth(),y,scale,boundX-this.second.getWidth(),boundY);
	this.second.render(data,x,y,scale,this.second.getWidth(),boundY);
}

HorizontalPair.prototype.getWidth = function() {
	return this.first.getWidth() + this.first.getWidth();
}

HorizontalPair.prototype.getHeigth = function() {
	return Math.max(this.first.getHeigth(),this.second.getHeigth());
}

// Represent a pair of objects place vertically next to each other
function VerticalPair(first, second) {
    ASTNode.call(this, "VerticalPair");
    this.first = first;
    this.second = second;
}
VerticalPair.prototype = Object.create(ASTNode.prototype);
VerticalPair.prototype.render = function(data,x,y,scale,boundX,boundY) {
	this.first.render(data,x,this.second.getHeigth()+y,scale,boundX,boundY-this.second.getHeigth());
	this.second.render(data,x,y,scale,boundX,this.second.getHeigth());
}

VerticalPair.prototype.getWidth = function() {
	return Math.max(this.first.getWidth(),this.second.getWidth());
}

VerticalPair.prototype.getHeigth = function() {
	return this.first.getHeigth() + this.first.getHeigth();
}

// Represent a pair of objects place overlapping each other
function OverlapPair(first, second) {
    ASTNode.call(this, "OverlapPair");
    this.first = first;
    this.second = second;
}
OverlapPair.prototype = Object.create(ASTNode.prototype);
OverlapPair.prototype.render = function(data,x,y,scale,boundX,boundY) {
	this.first.render(data,x,y,scale,boundX,boundY);
	this.second.render(data,x,y,scale,boundX,boundY);
}

OverlapPair.prototype.getWidth = function() {
	return Math.max(this.first.getWidth(),this.second.getWidth());
}

OverlapPair.prototype.getHeigth = function() {
	return Math.max(this.first.getHeigth(),this.second.getHeigth());
}

// Scale the inner object by "scaleBy"
function Scale(inner,scaleBy) {
    ASTNode.call(this, "Scale");
    this.inner = inner;
	this.scaleBy = scaleBy;
}
Scale.prototype = Object.create(ASTNode.prototype);
Scale.prototype.render = function(data,x,y,scale,boundX,boundY) {
	this.inner.render(data,x,y,scale*this.scaleBy,boundX,boundY);
}

Scale.prototype.getWidth = function() {
	return this.inner.getWidth() * this.scaleBy;
}

Scale.prototype.getHeigth = function() {
	return this.inner.getHeigth() * this.scaleBy;
}

// Align inner object to top of bounded rectangle
function AlignTop(inner) {
    ASTNode.call(this, "AlignTop");
    this.inner = inner;
}
AlignTop.prototype = Object.create(ASTNode.prototype);
AlignTop.prototype.render = function(data,x,y,scale,boundX,boundY) {
	this.inner.render(data,x,y+boundY-this.getHeigth(),scale,boundX,this.getHeigth());
}

AlignTop.prototype.getWidth = function() {
	return this.inner.getWidth();
}

AlignTop.prototype.getHeigth = function() {
	return this.inner.getHeigth()
}

function BeginCompose(imagePath,width,height)
{
	return new Single(imagePath,width,height);
}

// Following helps in call chaining
ASTNode.prototype.after = function(imagePath,width,height) {
		return new HorizontalPair(this,new Single(imagePath,width,height))
	}
	
ASTNode.prototype.on = function(imagePath,width,height) {
		return new VerticalPair(this,new Single(imagePath,width,height))
	}
	
ASTNode.prototype.in = function(imagePath,width,height) {
		return new OverlapPair(new Single(imagePath,width,height),this)
	}
	
ASTNode.prototype.afterComposition = function(composition) {
		return new HorizontalPair(this,composition)
	}
	
ASTNode.prototype.onComposition = function(composition) {
		return new VerticalPair(this,composition)
	}
	
ASTNode.prototype.inComposition = function(composition) {
		return new OverlapPair(composition,this)
	}

ASTNode.prototype.scale = function(scaleBy) {
	return new Scale(this,scaleBy);
}

ASTNode.prototype.topAlign = function(scaleBy) {
	return new AlignTop(this);
}