"use strict";

mocha.setup("bdd");

describe("System", function() {
    it("Mocha Works!", function() {
    });
	
	it("Composition", function(){		
		var treeFruit = BeginCompose("./images/fruit.png",32,32).scale(2)
			.in("./images/tree.png",233,243);
			
		var sun = BeginCompose("./images/sun.jpg",64,64).topAlign();
		
		var scene = BeginCompose("./images/car.png",240,70)
			.afterComposition(treeFruit)
			.after("./images/mailbox.png",64,64)
			.on("./images/road.jpg",800,64)
			.inComposition(sun);
			
		scene.render([],0,0,1,800,600);
	});
});


