# Design Rationale
The end goal is to develop a language to render scenes using code
The user should be able to use familiar english terms like on,
after, in. The user should be able to use call chaining rather than write
verbose code.
They should be able to compose parent scenese from  
child scenese. They should be aligh objects rather than stick 
them to other objects

We will use a combination of deeply embebedded DSL where the scene will be
represented as an abstract syntax tree. The final rendering will happen
by traversing this tree and and using d3 to render the images.

Each operator type will be a new abstracty syntax node inherited from a base ASTNode.
They will differ in how they implement functions like render, getWidth, getHeight etc.
To make the job easy for user, we have add a call chaining interface.

User will render the final uber composition, and the entire scene will be rendered as instructued

# Screencast
demo.mp4

# Well Documented Code
## Some Sample Code
##### Sample Code 1 - Render single object
var scene = BeginCompose("./images/car.png",240,70);
scene.render([],0,0,1,800,600);
##### Sample Code 2 - Render multiple objects, combined with chain interface
var scene = BeginCompose("./images/car.png",240,70)
	.after("./images/mailbox.png",64,64)
	.on("./images/road.jpg",800,64)
scene.render([],0,0,1,800,600);
##### Sample Code 3 - Render Child Composition as part of Parent Composition
var tree = BeginCompose("./images/tree.png",233,243);
var scene = BeginCompose("./images/car.png",240,70)
	.afterComposition(tree);
scene.render([],0,0,1,800,600);
##### Sample Code 4 - Scale
var fruit = BeginCompose("./images/fruit.png",32,32).scale(2);
fruit.render([],0,0,1,800,600);
##### Sample Code 5 - Top Align
var sun = BeginCompose("./images/sun.jpg",64,64).topAlign();
sun.render([],0,0,1,800,600);
##### Sample Code 6 - All together
var treeFruit = BeginCompose("./images/fruit.png",32,32).scale(2)
.in("./images/tree.png",233,243);			
var sun = BeginCompose("./images/sun.jpg",64,64).topAlign();	
var scene = BeginCompose("./images/car.png",240,70)
.afterComposition(treeFruit)
.after("./images/mailbox.png",64,64)
.on("./images/road.jpg",800,64)
.inComposition(sun);		
scene.render([],0,0,1,800,600);

## Integration
See code.js