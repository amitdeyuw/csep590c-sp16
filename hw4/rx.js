
function Stream() {
    this.callbacks = []
	this.dataList = []
	this.maps = [];
	this.filters = [];
	this.firsts = [];
	this.flattens = [];
	this.copies = [];
	this.combines = [];
	this.zips = [];
	this.unzips = [];
	this.latestPushed = undefined;
	this.latestPushedInInterval = undefined;
	this.latestPushedStream = undefined;
	this.latestPushedStreamsCopies = [];
	this.name = undefined;
	this.uniques = [];
}

Stream.prototype = {};

Stream.prototype.setName = function(name) {
	this.name = name;
}

Stream.prototype.getName = function() {
	return this.name;
}

Stream.prototype.enter = function(fname) {
	console.log("+++ : " + this.name + " : "+fname);
}
Stream.prototype.exit = function(fname) {
	console.log("--- : " + this.name + " : "+fname);
}

Stream.prototype.subscribe = function(f) {
	this.callbacks.push(f)
}

Stream.prototype.getLatest = function() {
	return this.latestPushed;
}

Stream.prototype._push = function(data) {
	if(data instanceof Stream){
		var localLatestStream = this.latestPushedStream;
		if(localLatestStream != undefined) {		
			this.latestPushedStreamsCopies.forEach(function(lsc){
				localLatestStream.uncopy(lsc);				
			})
		}
		this.latestPushedStream=data;
		
		this.latestPushedStreamsCopies.forEach(function(lsc){
			data.copy(lsc);
		});
	}
	
	this.latestPushed = data;
	this.latestPushedInInterval = data;
	
	for(var i=0;i<this.zips.length;i++){
		var izip = this.zips[i];
		var B = izip[0]
		var f = izip[1];
		var zipped = izip[2];
		if(this.latestPushed!=undefined&&B.getLatest()!=undefined){
			zipped._push(f(this.latestPushed,B.getLatest()));
		}
	}
	
	for(var i=0;i<this.unzips.length;i++){
		var izip = this.unzips[i];
		var A = izip[0]
		var f = izip[1];
		var zipped = izip[2];
		if(A.getLatest()!=undefined&&this.latestPushed!=undefined){
			zipped._push(f(A.getLatest(),this.latestPushed));
		}
	}
	
	this.combines.forEach(function(combine){
		data.copy(combine);
		//combine.copy(data);
	});
	
	this.dataList.push(data);
	
	this.maps.forEach(function(map){
		var out = map[0];
		var f = map[1];
		out._push(f(data));
	});
		
	this.callbacks.forEach(function(callback){
		callback(data)
		});

	this.filters.forEach(function(filter){
		var out = filter[0];
		var f = filter[1];
		if(f(data))
			out._push(data);
	});
	
	this.flattens.forEach(function(flatten){
		
		data.forEach(function(arrayElement){
			flatten._push(arrayElement);
		});
	});
	
	this.firsts.forEach(function(first){
		first._push(data);
	});
	this.firsts = [];
	
	this.copies.forEach(function(copy){
		copy._push(data);
	});
	
	this.uniques.forEach(function(unique){
		var out = unique[0]; 
		var f = unique[1];
		var dict = unique[2];
		var fData = f(data);
		var key = fData.toString();
		if(dict[key]==undefined){
			dict[key]=data;
			out._push(data);
		}
	});
}

Stream.prototype._push_many = function(dataArray) {
	for(var i=0;i<dataArray.length;i++){
		this._push(dataArray[i]);
	}
}

Stream.prototype.first = function() {
	var out = new Stream();
	this.firsts.push(out);
	return out;
}

Stream.prototype.map = function(f) {
	var out = new Stream();
	this.maps.push([out,f]);
	return out;
}

Stream.prototype.filter = function(f) {
	var out = new Stream();
	this.filters.push([out,f]);
	return out;
}

Stream.prototype.flatten = function() {
	var out = new Stream();
	this.flattens.push(out);
	return out;
}

Stream.prototype.copy = function(s) {
	this.copies.push(s);
}

Stream.prototype.uncopy = function(s) {
	var index = this.copies.indexOf(s);
	this.copies.splice(index, 1);
}

Stream.prototype.join = function(b) {
	var out = new Stream();
	this.copy(out);
	b.copy(out);
	return out;
}

Stream.prototype.combine = function() {
	var out = new Stream();
	this.combines.push(out);
	return out;
}

Stream.prototype.zip = function(B,f){
	var out = new Stream();
	this.zips.push([B,f,out]);
	B.unzip(this,f,out);
	return out;
}

Stream.prototype.unzip = function(A,f,zipped){
	this.unzips.push([A,f,zipped]);
}
// PART 1 HERE

Stream.timer = function(n){
	var out = new Stream();
	setInterval(function(){
		out._push(new Date());
	},n);
	return out;
}

Stream.dom = function(elementName, eventName){
	var out = new Stream();
	elementName.on(eventName, function(generatedEvent) {
		out._push(generatedEvent);
	});
	return out;
}

Stream.url = function(url){
	var out = new Stream();
	$.get(url, function(json){
		out._push(json);
	},"json");
	return out;
}

Stream.prototype.throttle = function(n) {
	var out = new Stream();
	var thisStream = this;
	setInterval(function(){
		if(thisStream.latestPushedInInterval!=undefined){
			out._push(thisStream.latestPushedInInterval);
			thisStream.latestPushedInInterval=undefined;
		}
	},n);
	return out;
}

Stream.prototype.latest = function() {
	var out = new Stream();
	this.latestPushedStreamsCopies.push(out);
	return out;
}

Stream.prototype.unique = function(f) {
	var out = new Stream();
	this.uniques.push([out,f,{}]);
	return out;
}


var FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

window.WIKICALLBACKS = {}

function WIKIPEDIAGET(s, cb) {
    $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        jsonp: "callback",
        data: {
            action: "opensearch",
            search: s,
            namespace: 0,
            limit: 10,
        },
        success: function(data) {cb(data[1])},
    })
}

Stream.wrapWIKIPEDIAGET = function(s) {
	var out = new Stream();
	WIKIPEDIAGET(s, function(x){
		out._push(x)
	})
	return out;
}

var firedEvents = []

$(function() {
    // PART 2 INTERACTIONS HERE
	/*
	var streamTimer = Stream.timer(1000);
	streamTimer.subscribe(function(x){
		$("#time").text(x);
	});	
	*/
	var streamDom = Stream.dom($("#button"),"click");
	var count = 0;
	streamDom.subscribe(function(x){
		count++;
		$("#clicks").text(count);
	});
	
	var streamThrottle = Stream.dom($("#mousemove"),"mousemove")
		.throttle(1000)
		.subscribe(function(x){
			$("#mouseposition").text(x.pageX+","+x.pageY);
		});
	
	var minuteTimer = Stream.timer(1000);
	var sos911 = minuteTimer.map(function(x){
		return Stream.url(FIRE911URL);
	});
	
	var  sos911Latest = sos911
		.latest()
		.flatten()
		.unique(function(x){
			return x["id"]})
		.map(function(x){
			return x[3479077]})
		.filter(function(x){
			var ss = $("#firesearch").val();
			if (ss!="" && ss!=undefined)
				return x.toLowerCase().includes($("#firesearch").val().toLowerCase())
			else
				return true;
		});
		
	sos911Latest.subscribe(function(x){
		$("#fireevents").append($("<li></li>").text(x));
		firedEvents.push(x);
	});
	
	var streamThrottle = Stream.dom($("#firesearch"),"input")
		.subscribe(function(event){
			$("#fireevents").empty();
			firedEvents
				.filter(function(x){
					return x.toLowerCase().includes(event.target.value.toLowerCase())})
				.forEach(function(x){
					$("#fireevents").append($("<li></li>").text(x));
				})
		});
	
	var streamSearchInput = Stream.dom($("#wikipediasearch"),"input").throttle(100);
	var streamOfStream = streamSearchInput.map(function (eventInfo){
		$("#wikipediasuggestions").empty();
		return Stream.wrapWIKIPEDIAGET(eventInfo.target.value);		
	});

	var latest = streamOfStream.latest();
	latest.subscribe(function(x){
		x.forEach(function(xi){
			$("#wikipediasuggestions").append($("<li></li>").text(xi));
		})		
	})
});
