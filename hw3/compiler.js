var antlr4 = require('antlr4/index');
var HandlebarsLexer = require('HandlebarsLexer').HandlebarsLexer;
var HandlebarsParser = require('HandlebarsParser').HandlebarsParser;
var HandlebarsParserListener = require('HandlebarsParserListener').HandlebarsParserListener;

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._usedHelpers = [];
    this._usedBlockHelpers = [];
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };

    this.registerBlockHelper('each', function (ctx, body, list) {
        var result = ""
        for(var i = 0;i<list.length;i++)
        {
            result += body(list[i]);
        }
        return result;
    });

    this.registerBlockHelper('if', function (ctx, body, condition) {
        if(condition)
        {
            return body(ctx);
        }
        else
        {
            return "";
        }
    });

    this.registerBlockHelper('with', function (ctx, body, field) {
        return body(ctx[field]);
    });

    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};

function lastElement(array) {
    return array[array.length-1];
}

HandlebarsCompiler.prototype.pushScope = function () {
    this._bodyStack.push(`var ${this._outputVar} = "";\n`);
}

HandlebarsCompiler.prototype.popScope = function () {
    this._bodyStack[this._bodyStack.length-1] += `return ${this._outputVar};\n`;
    return this._bodyStack.pop();
}

HandlebarsCompiler.prototype.compile = function (template) {
    this._usedHelpers = [];
    this._bodyStack = [];
    this.pushScope();

    var chars = new antlr4.InputStream(template);
    var lexer = new HandlebarsLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.document();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    for(var i=0;i<this._usedHelpers.length;i++)
    {
        var functionName = this._usedHelpers[i];
        var functionBody = this._helpers.expr[functionName].toString();
        this._bodyStack[this._bodyStack.length-1] =
            "var __$" + functionName + " = " + functionBody + ";\n" + this._bodyStack[this._bodyStack.length-1];
    }

    for(var i=0;i<this._usedBlockHelpers.length;i++)
    {
        var functionName = this._usedBlockHelpers[i];
        var functionBody = this._helpers.block[functionName].toString();
        this._bodyStack[this._bodyStack.length-1] =
            "var __$" + functionName + " = " + functionBody + ";\n" + this._bodyStack[this._bodyStack.length-1];
    }

    var popped = this.popScope();
    console.log(popped);
    return new Function(this._inputVar, popped);
};

HandlebarsCompiler.prototype.append = function (expr) {
    this._bodyStack[this._bodyStack.length-1] += `${this._outputVar} += ${expr};\n`
};

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.append(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitExpressionID= function (ctx) {
    ctx.source = "__$ctx." + ctx.getText();
};

HandlebarsCompiler.prototype.exitExpressionLiteral= function (ctx) {
    ctx.source = ctx.getText();
};

HandlebarsCompiler.prototype.exitParenthesizedExpression = function(ctx) {
    ctx.source = ctx.getChild(1).source;
};

HandlebarsCompiler.prototype.exitHelperApplication = function (ctx) {
    var functionName = ctx.getChild(0).getChild(0).symbol.text;
    this._usedHelpers.push(functionName)
    var params = []
    for(var i=1;i<ctx.getChildCount();i++)
    {
        params.push(ctx.getChild(i).getChild(0).source);
    }
    ctx.source = "__$"+functionName+"(__$ctx,"+params.join()+")";
};

// Exit a parse tree produced by HandlebarsParser#nonHelperAppliocationExpression.
HandlebarsCompiler.prototype.exitNonHelperAppliocationExpression = function(ctx) {
    ctx.source = ctx.getChild(0).source;
};

HandlebarsCompiler.prototype.exitExpression = function(ctx) {
    ctx.source = ctx.getChild(0).source;
};

HandlebarsCompiler.prototype.exitExpressionElement= function (ctx) {
    ctx.source = ctx.getChild(1).source;
    this.append(ctx.source);
};

// Enter a parse tree produced by HandlebarsParser#blockBody.
HandlebarsCompiler.prototype.enterBlockBody = function(ctx) {
    this.pushScope();
};

// Exit a parse tree produced by HandlebarsParser#blockBody.
HandlebarsCompiler.prototype.exitBlockBody = function(ctx) {
    var popped = this.popScope();
    var _function = new Function(this._inputVar, popped);
    ctx.source = _function.toString();
};

// Exit a parse tree produced by HandlebarsParser#blockElement.
HandlebarsParserListener.prototype.exitBlockElement = function(ctx) {
    var blockStart = ctx.getChild(0).getChild(2).getChild(0).symbol.text;
    var blockEnd = ctx.getChild(2).getChild(2).symbol.text;
    if(blockStart!=blockEnd){
        throw "Block start '"+blockStart+"' does not match the block end '"+blockEnd+"'.";
    }
    this._usedBlockHelpers.push(blockStart)

    var params = [ctx.getChild(1).source]
    for(var i=3;i<ctx.getChild(0).getChildCount()-1;i++)
    {
        params.push(ctx.getChild(0).getChild(i).getChild(0).source);
    }
    ctx.source = "__$"+blockStart+"(__$ctx,"+params.join()+")";
    this.append(ctx.source);
};

// Enter a parse tree produced by HandlebarsParser#document.
HandlebarsCompiler.prototype.enterDocument = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#document.
HandlebarsCompiler.prototype.exitDocument = function(ctx) {
};


exports.HandlebarsCompiler = HandlebarsCompiler;
