parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement
    : rawElement textBrace+
    | textBrace+;


textBrace : TEXT|BRACE;

expressionID
    : ID;
expressionLiteral
    : STRING | INTEGER | FLOAT;

parenthesizedExpression
    : OPEN_PAREN expression CLOSE_PAREN;
helperApplication
    : expressionID nonHelperAppliocationExpression+;
nonHelperAppliocationExpression
    : parenthesizedExpression | expressionID | expressionLiteral;
expression returns [source]
    : helperApplication | nonHelperAppliocationExpression;

expressionElement
    : START expression END ;
commentElement : START COMMENT END_COMMENT ;

blockElement: openingPart blockBody closingPart;
openingPart : START BLOCK expressionID expression* END;
blockBody : element+;
closingPart : START CLOSE_BLOCK ID END;
