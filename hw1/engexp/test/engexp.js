///<reference path="../typings/main.d.ts"/>
"use strict";
var chai_1 = require("chai");
var engexp_1 = require("../src/engexp");
describe("EngExp", function () {
    it("should parse a basic URL", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(" ")
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("https://www.google.com/maps")).to.be.true;
    });
    it("should parse a disjunctive date pattern", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .digit().repeated(1, 2)
            .then("/")
            .then(new engexp_1.default().digit().repeated(1, 2))
            .then("/")
            .then(new engexp_1.default().digit().repeated(2, 4))
            .or(new engexp_1.default()
            .digit().repeated(1, 2)
            .then(" ")
            .then(new engexp_1.default().match("Jan").or("Feb").or("Mar").or("Apr").or("May").or("Jun")
            .or("Jul").or("Aug").or("Sep").or("Oct").or("Nov").or("Dec"))
            .then(" ")
            .then(new engexp_1.default().digit().repeated(2, 4)))
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("12/25/2015")).to.be.true;
        chai_1.expect(e.test("25 Dec 2015")).to.be.true;
    });
    it("should capture nested groups", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        var result = e.exec("https://www.google.com/maps");
        chai_1.expect(result[1]).to.be.equal("google.com/maps");
        chai_1.expect(result[2]).to.be.equal("google.com");
    });
    it("should capture nested groups and work with disjunctions", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .or("https")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        var result = e.exec("https://www.google.com/maps");
        chai_1.expect(result[1]).to.be.equal("google.com/maps");
        chai_1.expect(result[2]).to.be.equal("google.com");
    });
    it("should capture from the beginning if unbalanced", function () {
        var e = new engexp_1.default().startOfLine().digit().oneOrMore().endCapture().then(" ")
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[1]).to.be.equal("15");
    });
    it("should capture till the end if unbalanced", function () {
        var e = new engexp_1.default().startOfLine().digit().oneOrMore().then(" ").beginCapture()
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[1]).to.be.equal("33");
    });
    it("should capture from the beginning of disjunction", function () {
        var e = new engexp_1.default().startOfLine()
            .digit()
            .then(new engexp_1.default().match("5").or("6").endCapture())
            .then(" ")
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[1]).to.be.equal("5");
    });
    it("should capture till the end of disjunction", function () {
        var e = new engexp_1.default().startOfLine()
            .digit()
            .then(new engexp_1.default().beginCapture().match("5").or("6"))
            .then(" ")
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[1]).to.be.equal("5");
    });
    it("should capture from the beginning of disjunction (nested)", function () {
        var e = new engexp_1.default().startOfLine()
            .digit()
            .then(new engexp_1.default().match("5").or("6").endCapture().endCapture().endCapture())
            .then(" ")
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[3]).to.be.equal("5");
    });
    it("should capture till the end of disjunction (nested)", function () {
        var e = new engexp_1.default().startOfLine()
            .digit()
            .then(new engexp_1.default().beginCapture().beginCapture().beginCapture().match("5").or("6"))
            .then(" ")
            .then(new engexp_1.default().digit().oneOrMore()).asRegExp();
        console.log(e);
        var result = e.exec("15 33");
        chai_1.expect(result[3]).to.be.equal("5");
    });
    it("should handle empty regexes", function () {
        var e = new engexp_1.default().asRegExp();
        console.log(e);
        chai_1.expect(e.test("")).to.be.true;
    });
    it("should separate alternatives", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .digit().or("a")
            .then(" ")
            .then(new engexp_1.default().digit().or("b"))
            .or("z")
            .endOfLine()
            .asRegExp();
        console.log(e);
        chai_1.expect(e.test("a b"), "should match 'a b'").to.be.true;
        chai_1.expect(e.test("1 b"), "should match '1 b'").to.be.true;
        chai_1.expect(e.test("a 1"), "should match 'a 1'").to.be.true;
        chai_1.expect(e.test("1 1"), "should match '1 1'").to.be.true;
        chai_1.expect(e.test("z"), "should match 'z'").to.be.true;
        chai_1.expect(e.test("a a"), "should not match 'a a'").to.be.false;
        chai_1.expect(e.test("b b"), "should not match 'b b'").to.be.false;
        chai_1.expect(e.test("b z"), "should not match 'b z'").to.be.false;
        chai_1.expect(e.test("bz"), "should not match 'bz'").to.be.false;
        chai_1.expect(e.test("az"), "should not match 'az'").to.be.false;
        chai_1.expect(e.test("a b z"), "should not match 'a b z'").to.be.false;
        chai_1.expect(e.test("abz"), "should not match 'abz'").to.be.false;
    });
});
//# sourceMappingURL=engexp.js.map