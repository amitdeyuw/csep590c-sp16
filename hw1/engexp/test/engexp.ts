///<reference path="../typings/main.d.ts"/>

import {expect} from "chai";
import EngExp from "../src/engexp";

describe("EngExp", () => {
    it("should parse a basic URL", () => {
        let e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(" ")
            .endOfLine()
            .asRegExp();
        expect(e.test("https://www.google.com/maps")).to.be.true;
    });

    it("should parse a disjunctive date pattern", () => {
        let e = new EngExp()
            .startOfLine()
            .digit().repeated(1, 2)
            .then("/")
            .then(new EngExp().digit().repeated(1, 2))
            .then("/")
            .then(new EngExp().digit().repeated(2, 4))
            .or(
                new EngExp()
                    .digit().repeated(1, 2)
                    .then(" ")
                    .then(
                        new EngExp().match("Jan").or("Feb").or("Mar").or("Apr").or("May").or("Jun")
                            .or("Jul").or("Aug").or("Sep").or("Oct").or("Nov").or("Dec")
                    )
                    .then(" ")
                    .then(new EngExp().digit().repeated(2, 4))
            )
            .endOfLine()
            .asRegExp();
        expect(e.test("12/25/2015")).to.be.true;
        expect(e.test("25 Dec 2015")).to.be.true;
    });

    it("should capture nested groups", () => {
        let e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        let result = e.exec("https://www.google.com/maps");
        expect(result[1]).to.be.equal("google.com/maps");
        expect(result[2]).to.be.equal("google.com");
    });
@<<<<<<< HEAD
    
    it("recursive or", () => {
        let e = new EngExp()
            .startOfLine()
            .then("large")
                .or(new EngExp().match("medium"))
                .or(new EngExp().match("small"))
            .then(" ")
            .then(new EngExp().match("fries").or("drink"))            
            .endOfLine()
            .asRegExp();
        expect(e.test("large fries")).to.be.true;
        expect(e.test("small drink")).to.be.true;
    });
    
    it("should capture non-ended group", () => {
        let e = new EngExp()
            .startOfLine()
            .then("quick")
            .beginCapture()
            .then("brown")
            .then("fox")
            .endOfLine()
            .asRegExp();
        let result = e.exec("quickbrownfox");
        expect(result[1]).to.be.equal("brownfox");     
    });
    
    it("should capture non-started group", () => {
        let e = new EngExp()
            .startOfLine()
            .then("quick")            
            .then("brown")
            .endCapture()
            .then("fox")
            .endOfLine()
            .asRegExp();
        let result = e.exec("quickbrownfox");
        expect(result[1]).to.be.equal("quickbrown");     
    });
    
    it("composable non-ended group", () => {
        let e = new EngExp()
            .startOfLine()
            .then("quick")            
            .then("brown")
            .then("fox")
            .or(new EngExp().then("lazy").beginCapture().then("dog"))
            .then("jumps")
            .then("over")
            .endOfLine()
            .asRegExp();
        let result = e.exec("lazydogjumpsover");
        expect(result[1]).to.be.equal("dog");     
    });
    
    it("or not from beginning", () => {
        let e = new EngExp()
            .startOfLine()
            .then("the")
            .then(
                    new EngExp().then("quick").then("brown").then("fox")
                .or(
                    new EngExp().then("lazy").then("dog")))
            .then("jumps")
            .then("over")
            .endOfLine()
            .asRegExp();
         expect(e.test("thelazydogjumpsover")).to.be.true; 
         expect(e.test("thequickbrownfoxjumpsover")).to.be.true; 
    });
    
    it("composable non-started group", () => {
        let e = new EngExp()
            .startOfLine()
            .then("the")
            .then(
                    new EngExp().then("quick").then("brown").then("fox")
                .or(
                    new EngExp().then("lazy").endCapture().then("dog")))
            .then("jumps")
            .then("over")
            .endOfLine()
            .asRegExp();
         let result = e.exec("thelazydogjumpsover");
         expect(result[1]).to.be.equal("lazy");             
    });
@=======

    it("should capture nested groups and work with disjunctions", () => {
        let e = new EngExp()
            .startOfLine()
            .then("http")
            .or("https")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        let result = e.exec("https://www.google.com/maps");
        expect(result[1]).to.be.equal("google.com/maps");
        expect(result[2]).to.be.equal("google.com");
    });


    it("should capture from the beginning if unbalanced", () => {
        let e = new EngExp().startOfLine().digit().oneOrMore().endCapture().then(" ")
            .then(new EngExp().digit().oneOrMore()).asRegExp();
        console.log(e);
        let result = e.exec("15 33");
        expect(result[1]).to.be.equal("15");
    });

    it("should capture till the end if unbalanced", () => {
        let e = new EngExp().startOfLine().digit().oneOrMore().then(" ").beginCapture()
            .then(new EngExp().digit().oneOrMore()).asRegExp();
        console.log(e);
        let result = e.exec("15 33");
        expect(result[1]).to.be.equal("33");
    });

    it("should capture from the beginning of disjunction", () => {
        let e = new EngExp().startOfLine()
            .digit()
            .then(new EngExp().match("5").or("6").endCapture())
            .then(" ")
            .then(new EngExp().digit().oneOrMore()).asRegExp();
        console.log(e);
        let result = e.exec("15 33");
        expect(result[1]).to.be.equal("5");
    });

    it("should capture till the end of disjunction", () => {
        let e = new EngExp().startOfLine()
            .digit()
            .then(new EngExp().beginCapture().match("5").or("6"))
            .then(" ")
            .then(new EngExp().digit().oneOrMore()).asRegExp();console.log(e);
        let result = e.exec("15 33");
        expect(result[1]).to.be.equal("5");
    });

    it("should capture from the beginning of disjunction (nested)", () => {
        let e = new EngExp().startOfLine()
            .digit()
            .then(new EngExp().match("5").or("6").endCapture().endCapture().endCapture())
            .then(" ")
            .then(new EngExp().digit().oneOrMore()).asRegExp();
        console.log(e);
        let result = e.exec("15 33");
        expect(result[3]).to.be.equal("5");
    });

    it("should capture till the end of disjunction (nested)", () => {
        let e = new EngExp().startOfLine()
            .digit()
            .then(new EngExp().beginCapture().beginCapture().beginCapture().match("5").or("6"))
            .then(" ")
            .then(new EngExp().digit().oneOrMore()).asRegExp();console.log(e);
        let result = e.exec("15 33");
        expect(result[3]).to.be.equal("5");
    });

    it("should handle empty regexes", () => {
        let e = new EngExp().asRegExp();
        console.log(e);
        expect(e.test("")).to.be.true;
    });

    it("should separate alternatives", () => {
        let e = new EngExp()
            .startOfLine()
            .digit().or("a")
            .then(" ")
            .then(new EngExp().digit().or("b"))
            .or("z")
            .endOfLine()
            .asRegExp();
        console.log(e);
        expect(e.test("a b"), "should match 'a b'").to.be.true;
        expect(e.test("1 b"), "should match '1 b'").to.be.true;
        expect(e.test("a 1"), "should match 'a 1'").to.be.true;
        expect(e.test("1 1"), "should match '1 1'").to.be.true;
        expect(e.test("z"), "should match 'z'").to.be.true;
        expect(e.test("a a"), "should not match 'a a'").to.be.false;
        expect(e.test("b b"), "should not match 'b b'").to.be.false;
        expect(e.test("b z"), "should not match 'b z'").to.be.false;
        expect(e.test("bz"), "should not match 'bz'").to.be.false;
        expect(e.test("az"), "should not match 'az'").to.be.false;
        expect(e.test("a b z"), "should not match 'a b z'").to.be.false;
        expect(e.test("abz"), "should not match 'abz'").to.be.false;
    })
@>>>>>>> remotes/csep590c_sp16_overlord/csep590c-sp16/master
});
