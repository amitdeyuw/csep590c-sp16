/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 */

//// The AST

// This class represents all AST Nodes.
function ASTNode(type) {
    this.type = type;
}

ASTNode.prototype = {};

// The All node just outputs all records.
function AllNode() {
    ASTNode.call(this, "All");
}

// This is how we make AllNode subclass from ASTNode.
AllNode.prototype = Object.create(ASTNode.prototype);

// The Filter node uses a callback to throw out some records.
function FilterNode(callback) {
    ASTNode.call(this, "Filter");
    this.callback = callback;
}

FilterNode.prototype = Object.create(ASTNode.prototype);

// The Then node chains multiple actions on one data structure.
function ThenNode(first, second) {
    ASTNode.call(this, "Then");
    this.first = first;
    this.second = second;
}

ThenNode.prototype = Object.create(ASTNode.prototype);




//// Executing queries

ASTNode.prototype.execute = function(table) {
    throw new Error("Unimplemented AST node " + this.type)
}

AllNode.prototype.execute = function(table) {
	//console.log("all")
	//console.log(table.length)
    return table.slice();
}

FilterNode.prototype.execute = function(table) {
	//console.log("filter.execute")
	//console.log("input")
	//console.log(table)
    var output = []
	for(i=0;i<table.length;i++)
	{
		if(this.callback(table[i]))
		{
			output.push(table[i]);
		}
	}
	return output;
}

ThenNode.prototype.execute = function(table) {
	return this.second.execute(this.first.execute(table).slice());
}



// ...




//// Write a query
// Define the `thefts_query` and `auto_thefts_query` variables

var thefts_query = new FilterNode(function(x){
		return x[13].toLowerCase().includes("theft")
		});

var auto_thefts_query = new FilterNode(function(x){return x[13].toLowerCase().includes("theft")&&x[13].toLowerCase().includes("veh")});




//// Add Apply and Count nodes

// ...
// The Filter node uses a callback to throw out some records.
function ApplyNode(callback) {
    ASTNode.call(this, "Apply");
    this.callback = callback;
}

ApplyNode.prototype = Object.create(ASTNode.prototype);

ApplyNode.prototype.execute = function(table) {
	//console.log("apply.execute")
	//console.log("input")
	//console.log(table)
	var output = []
	for(i=0;i<table.length;i++)
	{
		//console.log("table[i]")
		//console.log(table[i])
		//console.log("cbr")
		//console.log(this.callback(table[i]))
		output.push(this.callback(table[i]));
	}
	//console.log("output")
	//console.log(output)
	return output;
}

function CountNode(callback) {
    ASTNode.call(this, "Count");
}

CountNode.prototype = Object.create(ASTNode.prototype);

CountNode.prototype.execute = function(table) {
	//console.log("count")
	//console.log(table.length)
	return [table.length];
}


//// Clean the data

var cleanup_query = new ApplyNode(function(x){return {type: x[13], description: x[15], date: x[17], area: x[18]}});


//// Implement a call-chaining interface

// ...

var qc = (function (){
	function qc() {
		this.astNode = new AllNode();
	}
	function qc(astNode) {
		this.astNode = astNode;
	}
	qc.prototype.apply = function(callback) {
		this.astNode=(new ThenNode(this.astNode,new ApplyNode(callback)))
		return this
	}
	qc.prototype.filter = function(filter) {
		this.astNode=(new ThenNode(this.astNode,new FilterNode(filter)))
		return this
	}
	qc.prototype.count = function() {
		this.astNode=(new ThenNode(this.astNode,new CountNode()))
		return this
	}
	qc.prototype.execute = function(table) {
		return this.astNode.execute(table);
	}
	return qc;
}())

// Q = new qc()
Q = Object.create(new AllNode());

ASTNode.prototype.filter = function(filter) {
		return new ThenNode(this,new FilterNode(filter))
	}
ASTNode.prototype.apply = function(callback) {
		return new ThenNode(this,new ApplyNode(callback))
	}
ASTNode.prototype.count = function() {
		return new ThenNode(this,new CountNode())
	}
//// Reimplement queries with call-chaining

var cleanup_query_2 = Q.apply(function(x){return {type: x[13], description: x[15], date: x[17], area: x[18]}})

var thefts_query_2 = Q.filter(function(x){
		return x["type"].toLowerCase().includes("theft")
		})

var auto_thefts_query_2 = thefts_query_2.filter(function(x){return x["type"].toLowerCase().includes("veh")})

//// Optimize filters

ASTNode.prototype.optimize = function() { return this; }

ThenNode.prototype.optimize = function() {
	if(this.second.type=="Filter"
		&&this.first.type=="Then"
		&&this.first.second.type=="Filter")
	{
		var f = this.first.second.callback;
		var g = this.second.callback;
		var x = this.first.first;
		var faag = function(x) { return f(x)&&g(x)}
		return new ThenNode(x,new FilterNode(faag));
	}
	if(this.second.type=="Count"
		&&this.first.type=="Then"
		&&this.first.second.type=="Filter")
	{
		var f = this.first.second.callback;
		var x = this.first.first;
		return new ThenNode(x,new CountIfNode(f));
	}
	/*if(this.second.type=="Apply"
		&& this.second.callback==am
		&& this.first.type=="Then"
		&& this.first.first.type=="CartesianProduct"
		&& this.first.second.type=="Filter"*/
    return new ThenNode(this.first.optimize(), this.second.optimize())
}

// We add a "run" method that is like "execute" but optimizes queries first.

ASTNode.prototype.run = function(data) {
    this.optimize().execute(data);
}

function AddOptimization(node_type, f) {
    var old = node_type.prototype.optimize;
    node_type.prototype.optimize = function() {
        var new_this = old.apply(this, arguments);
        return f.apply(new_this, arguments) || new_this;
    }
}

// ...




//// Internal node types and CountIf

// ...

function CountIfNode(filter) {
    ASTNode.call(this, "CountIf");
	this.filter = filter;
}
CountIfNode.prototype = Object.create(ASTNode.prototype);
CountIfNode.prototype.execute = function(table) {
    var output = []
	for(i=0;i<table.length;i++)
	{
		if(this.filter(table[i]))
		{
			output.push(table[i]);
		}
	}
	return output.length;
}
//ASTNode.prototype.countIf = function(filter) {
//		return new ThenNode(this,new CountIfNode(filter))
//	}

//// Cartesian Products

// ...
function CartesianProductNode(left,right) {
    ASTNode.call(this, "CartesianProduct");
	this.left = left;
	this.right = right;
}
CartesianProductNode.prototype = Object.create(ASTNode.prototype);
CartesianProductNode.prototype.execute = function(table) {
    var output = []
	var outputL = this.left.execute(table)
	var outputR = this.right.execute(table)
	for(i=0;i<outputL.length;i++)
	{
		for(j=0;j<outputR.length;j++)
		{
			output.push({left:outputL[i],right:outputR[j]})
		}
	}
	//console.log("cp.execute");
	//console.log(output)
	return output;
}


//// Joins

// ...
function merge(l,r) {
	var joined = {}
	for(var key in Object.keys(r)){
		if(!Object.keys(joined).includes(key)){
			joined[key] = r[key]
		}
	}
	for(var key in Object.keys(l)){
		if(!Object.keys(joined).includes(key)){
			joined[key] = l[key]
		}
	}
	return joined	
}

function JoinNode(f,l,r) {
    ASTNode.call(this, "Join");
	this.f = f;
	this.l = l;
	this.r = r;
}
JoinNode.prototype = Object.create(ASTNode.prototype);
JoinNode.prototype.execute = function(table) {
    var output = []
	var outputL = this.l.execute(table)
	var outputR = this.r.execute(table)
	for(i=0;i<outputL.length;i++)
	{
		for(j=0;j<outputR.length;j++)
		{
			if(f(outputL[i],outputR[j])){
				output.push(merge(outputL[i],outputR[j]))
			}
		}
	}
	return output;
}


function am(x){
	return merge(x["left"],x["right"])
}

ASTNode.prototype.product = function(l,r) {
		return new CartesianProductNode(l,r)
	}
ASTNode.prototype.join = function(f,l,r) {
	return new ThenNode(
			new ThenNode(
				new CartesianProductNode(l,r),
				new FilterNode(function(x){
					return f(x["left"],x["right"])
				})
			),
			new ApplyNode(am)
		)
}

//// Optimizing joins

// ...




//// Join on fields

// ...




//// Implement hash joins

// ...

ASTNode.prototype.on = function(field) {
return function(l, r) { 
	return l[field]==r[field] 
}
}


//// Optimize joins on fields to hash joins

// ...
